# Introduction

This is a collection of configuration files for several programs. The goal is
to restore and/or share common application settings between different machines
and getting quickly the same working environment.

Most tools need a configuration file or folder in your home directory. These
are commonly hidden files or directories and this is why this collection is
called `dotfiles`.

# Quick Install

It is not important where you have stored the `dotfiles`. Place it where ever
you want like `~/dotfiles` or `~/etc/dotfiles` or `/opt/dotfiles/`. Also the
name doesn't matter, therefore it can be hidden itself like `~/.dotfiles`.

Be sure you have some prerequisites installed:
- bash
- bash-completion
- dircolors
- vim
- nvim
- tmux
- git
- curl

Some optionals:
- svn
- universal-ctags

It is expected you use a 256 color terminal like `gnome-terminal` or `konsole`
and you use the solarized dark color theme:
[Solarized](https://ethanschoonover.com/solarized/)

For installation example it is assumed you cloned the `dotfiles` directly into
your home directory:

```
cd
ln -s dotfiles/vim/.vimrc
ln -s dotfiles/vim/.vim
ln -s dotfiles/neovim/nvim .config/nvim
ln -s ~/dotfiles/neovim/nvim ~/.config/nvim
ln -s dotfiles/tmux/.tmux.conf
ln -s dotfiles/bash-completion/.bash_completion
ln -s dotfiles/universal-ctags/.ctags.d
echo 'source dotfiles/bash/bashrc' >> .bashrc
```

Then start `vim` and let the show begin. After that start `tmux` and hit
`<prefix> I`.

# Description

In this `dotfiles` collection each program has it's own directory where the
configuration files and/or folders are stored. The idea is to link the
configuration file/folder to your home directory.

In some cases you have to copy configuration snippets into an existing
configuration file or to source the snippets.

See the following list of available tools and how to install the
configurations.

## bash

### bashrc

You can use the bash configuration file `bash/bashrc` from the `dotfiles` by
linking it to your home directory. Don't forget to link as a hidden file.

But usually your OS distribution already placed a bash configuration file in
your home directory (`~/.bashrc`) with more or less default settings in it.
Therefore, you can source the `bash/bashrc` inside your existing `~/.bashrc`.

The `bash/bashrc` contains some common settings like the `EDITOR` variable,
alias definitions and adds some 3rd party extensions. Also a `tmux` helper
function is included (see section tmux for details).

If you don't want to import all this stuff to your `~/.bashrc` you can see the
following subsections to enable only a few things.

### Aliases

If you don't want to use `bash/bashrc` but you want to get the alias
definitions you can use them by sourcing `bash/bash_aliases`. Add `source
<DOTFILES>/bash/bash_aliases` in your bash configuration file `~/.bashrc`.

If `dircolors` is installed you get aliases for`ls` and `grep` where colorized
output is enabled. There are some more aliases for listing directories and
files.

### Extensions

If you don't want to use `bash/bashrc` but you want to get all the 3rd party
extensions you can use them by sourcing `bash/extensions/enable-all`. Add
`source <DOTFILES/bash/extensions/enable-all` in your bash configuration file
`~/.bashrc`. See the following subsection for details and how to get only a
subset of the 3rd party extensions.

#### Prompt

You get a very nice bash prompt especially if you are inside a git working
copy: [bash-git-prompt](https://github.com/magicmonty/bash-git-prompt).

Just source `bash/extensions/bash-git-prompt/enable-me`. At the very first
sourcing the extension is automatically cloned from github.com. You can add
`source <DOTFILES>/bash/extensions/bash-git-prompt/enable-me` inside your bash
configuration file `~/.bashrc`.

#### Solarized dircolors

You get a solarized color output for `ls`:
[dircolors-solarized](https://github.com/seebi/dircolors-solarized).

Just source `bash/extensions/dircolors-solarized/enable-me`. At the very first
sourcing the extension is automatically cloned from github.com. You can add
`source <DOTFILES>/bash/extensions/dircolors-solarized/enable-me` inside your
bash configuration file `~/.bashrc`.

The extension is working only if you have installed `dircolors`.

## bash-completion

It provides additional completions for [bash-completion](https://github.com/scop/bash-completion).

If it is installed there should be following entry in `/etc/bash.bashrc`:
```
[ -r /usr/share/bash-completion/bash_completion   ] && .  /usr/share/bash-completion/bash_completion
```
If this line is not present add it to your user configuration file `~/.bashrc`.

Additional user completions will be automatically added if there is a user
configuration file `~/.bash_completion`.

Therefore, just link `bash-completion/.bash_completion` from the `dotfiles` to
your home directory.

Currently following addtional completions are available:
- tmux<br />
  This is just a copy from
  [tmux-bash-completion](https://github.com/imomaliev/tmux-bash-completion)
  without any modifications (version date 2020-04-19).
  Please note this is released under GPL2.
  This is itself a modifacation of
  [bash-it's tmux completion](https://github.com/Bash-it/bash-it/blob/master/completion/available/tmux.completion.bash).

- svn<br />
  This is just a copy from
  [Apache Subversion Tools](https://svn.apache.org/repos/asf/subversion/trunk/tools/client-side/bash_completion)
  without any modifacations (version date 2020-03-31).
  Please note this is released under Apache License Version 2.0.

## tmux

### Installation

Just link `tmux/.tmux.conf` from the `dotfiles` to your home directory.

### Plugins

On every start of `tmux` it will be checked if the plugin manager
[tpm](https://github.com/tmux-plugins/tpm) is installed to
`~/.tmux/plugins/tpm`. If not it will be automatically cloned from github.
Following prerequisites must be installed on your host machine:
- `git`

On the very first start just install the plugins with `<prefix> I` (`Ctrl-b I`).

### Environment

TL;DR: `echo 'source <DOTFILES>/tmux/add-environment-update' >> ~/.bashrc`, if
you are not using `bash/bashrc` from the `dotfiles`.

Once you started a tmux session and some shells in it, the shells inherited the
environment. Running shells never update the environment. Imagine you login to
a server via `ssh` and attach to a running tmux session, the `DISPLAY` variable
may has been changed. In your already running shell the `DISPLAY` variable
differs from your new login shell. X forwarding will not working. If you spwan
a new shell inside the tmux session, the `DISPLAY` variable is set correctly.

In an existing running shell inside the tmux session you can run `tmux
show-environment` to see the environment of your login shell. So you can
compare and change the environment of the running shell.

You can find `tmux/add-environment-update` inside the `dotfiles` to easily update the
environment of an existing running shell inside a tmux session. It provides a
bash function `tmux`. If you call it with the first argument is
`update-environment` or `update-env` or `env-update` your environment gets an
update. In all other cases `tmux` itself is called with the given arguments.
The function acts as a wrapper for the `tmux` binary in order to add new
commands.

To be able to call the function in all your shells add `source
<DOTFILES>/tmux/add-environment-update` in your bash configuration file
`~/.bashrc`. The `bash/bashrc` from the `dotfiles` already contains this
entry.

## universal-ctags

It provides additional tags for
[universal-ctags](https://github.com/universal-ctags/ctags) or extends already
existing once.

If it is installed 'u-ctags' looks inside `~/ctags.d/` for existing `*.ctags`
files. Therfore, just link `universal-ctags/.ctags.d` to your home directory.

Currently following user definitions are available:
- `vhdl.ctags`<br /> It extends the existing VHDL parser by the kinds 'ports',
  'signals', 'processes' and 'instances'.

## vim

### Installation

Just link `vim/.vimrc` and `vim/.vim` from the `dotfiles` to your home
directory.

### Plugins

At the very first start of `vim` all plugins will be installed automatically.
Following prerequisites must be installed on your host machine:
- `curl`
- `git`

The plugin manager `vim-plug` is used. Update your plugins with vim command
`:PlugUpdate`.

### Extension

There are some errorformats used for HDL developement. See `vim/.vim/compiler`
or type `:compiler` and double tab for a complete list of available compiler
errorformats. Following compilers are added for the QuickFix:
- QuestaSim
- VCS-MX

To activate use vim commands `:compiler! questa` or `:compile! vcs`. It is
assumed you use `make` for compiling HDL sources. Then the error parsing is
done automatically after vim command `:make`.

If you use shell scripts or simulator specific scripts (like `do` files) you
can redirect the output to a file. After compilation you can read the file into
your quickfix with `:cf <FILE>`.

## neovim

currently under construction ...

# License

This `dotfiles` collection distribute some foreign work under specific license
terms:

- `bash-completion/completions/tmux`
  - Source: [tmux-bash-completion](https://github.com/imomaliev/tmux-bash-completion)
  - State: no modifications
  - Download: 2020-04-19.
  - License: GPL2

- `bash-completion/completions/svn`
  - Source: [Apache Subversion Tools](https://svn.apache.org/repos/asf/subversion/trunk/tools/client-side/bash_completion)
  - State: no modifications
  - Download: 2020-03-31
  - License: Apache License 2.0

- `tmux/tmux.sh`
  - Source: [raimue.blog](https://raimue.blog/2013/01/30/tmux-update-environment/)
  - State: no modifications
  - Download: 2020-05-09
  - License: Public Domain

Other stuff of the `dotfiles` collection is created by myself and I publish it
under MIT License.

# ToDo

- vim
  - split vimrc into vimrc.plugins and vimrc.core
  - move .vimrc to .vim/vimrc to be able to linking .vim only

- universal-ctags
  - fix regex for instances

# Ideas

- powerline for bash
- zsh
