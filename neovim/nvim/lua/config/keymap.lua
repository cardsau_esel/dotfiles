-- simple move between splits. just hit CTRL+[hjkl]
vim.keymap.set( 'n', '<C-j>', '<C-W><C-j>' )
vim.keymap.set( 'n', '<C-k>', '<C-W><C-k>' )
vim.keymap.set( 'n', '<C-l>', '<C-W><C-l>' )
vim.keymap.set( 'n', '<C-h>', '<C-W><C-h>' )

-- added new short cuts 'gb' and 'gB' to switch through the buffers
-- like 'gt' and 'gT' to move through tabs
vim.keymap.set( 'n', 'gb', '<Cmd>bnext<CR>' )
vim.keymap.set( 'n', 'gB', '<Cmd>bprevious<CR>' )
