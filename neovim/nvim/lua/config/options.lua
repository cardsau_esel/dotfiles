-- Enable filetype detection (common: unix, DOS, Mac / special: Makefile, git-commit-message, ...)
-- and load plugin and indent file for this type, if available.
vim.cmd.filetype( { 'plugin', 'indent', 'on' } )

-- Enable syntax highlighting, but don't override with vim defaults
-- and therefore ':highlight' command can be used before
vim.cmd.syntax( { 'enable' } )


-- enable line number
vim.opt.number = true
-- enable a line which marks the active line
vim.opt.cursorline = true
-- enable a line which marks the column to stop to write
vim.opt.colorcolumn = '80'
-- enable the status line always
vim.opt.laststatus = 2
-- increase command line height
vim.opt.cmdheight = 2


-- Set tab stop width and shift width (auto indent: << >>) to 4
-- and converts tabs to spaces in insert mode.
-- Use ':retab' for converting existing files from tabs to spaces.
-- To insert a tab sign in insert mode: <C-v><Tab>
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
-- enable auto indent if starting a new line with 'o' or 'O'
vim.opt.autoindent = true


-- When split (':split'), the new window shall spawn below the current one.
vim.opt.splitbelow = true
-- When split vertically (':vsplit'), the new window shall spawn right the
-- current one.
vim.opt.splitright = true


-- Map special whitespace characters to printable characters.
-- Use ':set list' to display the characters.
-- Use ':set nolist' to turn it off.
vim.opt.listchars = { eol = '$', tab = '»-', trail = '·', space = '·', extends = '>', precedes = '<', nbsp = '~' }
-- Use background color for "nbsp", "space", "tab" and "trail".
vim.cmd.highlight( { 'SpecialKey', 'ctermbg=8' } )


-- Set curly braces to be an allowed character for file names.
-- And now, 'gf' works on environment variables with curly braces too.
vim.opt.isfname:append( { '{', '}' } )
