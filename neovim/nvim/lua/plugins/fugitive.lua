return {
    'tpope/vim-fugitive',

    enabled = true,

    lazy = false,
    priority = 1000,
}
