return {
    'airblade/vim-gitgutter',

    enabled = false,

    lazy = false,
    priority = 1000,
}
