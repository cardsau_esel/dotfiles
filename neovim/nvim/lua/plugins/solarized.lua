return {
    'maxmx03/solarized.nvim',

    enabled = true,

    lazy = false,
    priority = 1000,

    opts = {

        transparent = false, -- enable transparent background
        palette = 'solarized', -- or selenized
        styles = {
            comments = { --[[italic = true, bold = true, underline = true]] },
            functions = {},
            variables = {},
            numbers = {},
            constants = {},
            parameters = {},
            keywords = {},
            types = {},
        },
        enables = {
            editor = true,
            syntax = true,

            -- plugins
            bufferline = false,
            cmp = false,
            diagnostic = false,
            dashboard = false,
            gitsign = false,
            hop = false,
            indentblankline = false,
            lsp = false,
            lspsaga = false,
            navic = false,
            neogit = false,
            neotree = false,
            notify = false,
            noice = false,
            semantic = false,
            telescope = false,
            tree = false,
            treesitter = false,
            todo = false,
            whichkey = false,
            mini = false,
        },
        highlights = {},
        colors = {},
        theme = 'default', -- or 'neo'
        autocmd = true,
    },

    config = function( _, opts )

        -- configure plugin with opts
        require( 'solarized' ).setup( opts )

        -- enable the colorscheme
        vim.cmd.colorscheme( 'solarized' )
    end,
}
