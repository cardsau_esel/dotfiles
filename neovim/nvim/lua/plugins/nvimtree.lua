return {
    'nvim-tree/nvim-tree.lua',

    enabled = false,

    lazy = false,
    priority = 1000,

    dependencies = { 'nvim-tree/nvim-web-devicons' },

    opts = {},

    config = function( _, opts )
        require( 'nvim-tree').setup( opts )
    end
}
