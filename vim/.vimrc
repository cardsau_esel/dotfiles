""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin Manager
""""""""""""""""""""""""""""""""""""""""""""""""""

" this should be the very first which must be load/set
" before other settings are done. You can use
" Pathogen or vim-plug to manage the vim plugins.


"
" enable pathogen plugin
" https://github.com/tpope/vim-pathogen

" Download vim-plug if not already installed
"if empty(filereadable(expand('~/.vim/autoload/pathogen.vim')))
"    silent !mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
"    silent !git clone https://www.github.com/altercation/vim-colors-solarized ~/.vim/bundle/vim-colors-solarized
"    silent !git clone https://www.github.com/scrooloose/nerdtree ~/.vim/bundle/nerdtree
"    silent !git clone https://www.github.com/ivalkeen/nerdtree-execute ~/.vim/bundle/nerdtree-execute
"    silent !git clone https://www.github.com/kien/ctrlp.vim ~/.vim/bundle/ctrlp
"    silent !git clone https://www.github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
"    silent !git clone https://www.github.com/vim-airline/vim-airline-themes ~/.vim/bundle/vim-airline-themes
"    silent !git clone https://www.github.com/bling/vim-bufferline ~/.vim/bundle/vim-bufferline
"    "silent !git clone https://www.github.com/airblade/vim-gitgutter ~/.vim/bundle/vim-gitgutter " removed by signify
"    silent !git clone https://www.github.com/mhinz/vim-signify ~/.vim/bundle/vim-signify
"    silent !git clone https://www.github.com/tpope/vim-fugitive ~/.vim/bundle/vim-fugitive
"    silent !git clone https://www.github.com/tpope/vim-obsession ~/.vim/bundle/vim-obsession
"    silent !git clone https://www.github.com/mileszs/ack.vim ~/.vim/bundle/ack
"    silent !git clone https://www.github.com/vim-scripts/a.vim ~/.vim/bundle/a
"    silent !git clone https://www.github.com/majutsushi/tagbar ~/.vim/bundle/tagbar
"    silent !git clone https://www.github.com/godlygeek/tabular ~/.vim/bundle/tabular
"    silent !git clone https://www.github.com/nachumk/systemverilog.vim ~/.vim/bundle/systemverilog
"    silent !git clone https://www.github.com/christoomey/vim-tmux-navigator ~/.vim/bundle/vim-tmux-navigator
"    "silent !git clone https://www.github.com/henrik/vim-indexed-search ~/.vim/bundle/vim-indexed-search
"    silent !git clone https://www.github.com/t9md/vim-quickhl ~/.vim/bundle/vim-quickhl
"    silent !git clone https://www.github.com/troydm/zoomwintab.vim ~/.vim/bundle/zoomwintab
"    silent !git clone https://www.github.com/rust-lang/rust.vim.git ~/.vim/bundle/rust.vim
"    silent !git clone https://www.github.com/dominikduda/vim_current_word ~/.vim/bundle/vim_current_word
"endif
" "after that, a new plugin can be installed, if it
" "is copied to ~/.vim/bundle/<PLUGIN_NAME>
"execute pathogen#infect()


"
" enable vim-plug
" https://github.com/junegunn/vim-plug

" Download vim-plug if not already installed
if empty(filereadable(expand('~/.vim/autoload/plug.vim')))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
         \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    silent !mkdir -p ~/.vim/plugged
    autocmd VimEnter * PlugInstall
endif

" the given plugins will be automatically checked out from github
call plug#begin('~/.vim/plugged')
"let $GIT_SSL_NO_VERIFY = 'true'
    Plug 'altercation/vim-colors-solarized'
    Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
    Plug 'ivalkeen/nerdtree-execute'
    Plug 'kien/ctrlp.vim'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'bling/vim-bufferline'
    "Plug 'airblade/vim-gitgutter' " removed by signify
    Plug 'mhinz/vim-signify'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-obsession'
    Plug 'mileszs/ack.vim'
    Plug 'vim-scripts/a.vim'
    Plug 'majutsushi/tagbar'
    Plug 'godlygeek/tabular'
    Plug 'nachumk/systemverilog.vim'
    Plug 'christoomey/vim-tmux-navigator'
    "Plug 'henrik/vim-indexed-search'
    Plug 't9md/vim-quickhl'
    Plug 'troydm/zoomwintab.vim'
    Plug 'rust-lang/rust.vim'
    Plug 'dominikduda/vim_current_word'
call plug#end()

" Use following commands
":PlugInstall
":PlugUpdate


""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                "
"                  VIM Core                      "
"                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""
" Settings
""""""""""""""""""""""""""""""""""""""""""""""""""

" disable vi compatible mode and enable vim improvements
" e.g. arrow keys can be used :)
set nocompatible

" enable filetype detection (common: unix, DOS, Mac / special: Makefile, git-commit-message, ...)
" and load plugin and indent file for this type, if available
filetype plugin indent on

" enable syntax highlighting, but don't override with vim defaults
" and therefore ':highlight' command can be used before
syntax enable

" turn off autodetection for color scheme and use always light
" set background=light
" turn off autodetection for color scheme and use always dark
set background=dark
" tells vim that a terminal with 16 different colors is used
set t_Co=16
" use solarized colorscheme and tells the solarized plugin, that
" a terminal with 16 color is used, in which the solarized palette
" is already set and can be imported frome there
let g:solarized_termcolors=16
colorscheme solarized

" enable line number
set number

" enable a line which marks the active line
set cursorline
" choose background color for cursor line
" since colorscheme solarized is used before, it is already set
" to a good color and must not be override
"highlight CursorLine term=reverse cterm=NONE ctermbg=12

" set tab stop width and shift width (auto indent: << >>) to 4
" and converts tabs to spaces in insert mode
" use ':retab' for converting existing files from tabs to spaces
" to insert a tab sign in insert mode: <C-v><Tab>
set tabstop=4
set shiftwidth=4
set expandtab

" enable auto indent if starting a new line with 'o' or 'O'
set autoindent

" enable backspacing in insert mode over autoindent, line breaks ans start of insert
set backspace=indent,eol,start

" enable command line completion by pressing <tab>
set wildmenu
" complete with longest common string and list all matches
set wildmode=longest,list

" don't wrap long lines
set nowrap

" modified buffer can be hidden, without writing it before
set hidden

" enable incremental search: jump to the next pattern match while
" typing the search command. Cursor isn't moved, so <Esc> will jump
" back. Move cursor to pattern match by finishin the search command with <Enter>
set incsearch
" highlight all pattern matches. Use ':noh' to turn it off.
set hlsearch

" When on, splitting a window will put the new window below the current
" one. |:split|
set splitbelow

" When on, splitting a window will put the new window right of the
" current one. |:vsplit|
set splitright

" enable mouse support in all modes
set mouse=a

" show status line for non split windows too
set laststatus=2

" map special whitespace characters to printable characters
" use 'set list' to display the characters
" use 'set nolist' to turn it off
set listchars=eol:$,tab:»-,trail:·,space:·,extends:>,precedes:<,nbsp:~
" use background color for "nbsp", "space", "tab" and "trail".
highlight SpecialKey ctermbg=8

" increase command line height
set cmdheight=2

" set curly braces to be an allowed character for file names
" now, 'gf' works on environment variables with curly braces too.
set isfname+={,}

""""""""""""""""""""""""""""""""""""""""""""""""""
" new shortcuts
""""""""""""""""""""""""""""""""""""""""""""""""""

" removed entering 'ex mode' when type 'Q'
nnoremap Q <nop>

" added new short cuts 'gb' and 'gB' to switch through the buffers
" like 'gt' and 'gT' to move through tabs
nnoremap gb :bnext<CR>
nnoremap gB :bprevious<CR>

" use CTRL-Space for auto completion too
if has("gui_running")
    " C-Space seems to work under gVim on both Linux and win32
    inoremap <C-Space> <C-p>
else " no gui
    if has("unix")
        " Terminal emulator translate CTRL-Space to NULL
        inoremap <Nul> <C-p>
    endif
endif

" simple move between splits. just hit CTRL+[hjkl]
" keymaps are already included in plugin 'vim-tmux-navigator'
" nnoremap <C-j> <C-W><C-j>
" nnoremap <C-k> <C-W><C-k>
" nnoremap <C-l> <C-W><C-l>
" nnoremap <C-h> <C-W><C-h>


""""""""""""""""""""""""""""""""""""""""""""""""""
" commands
""""""""""""""""""""""""""""""""""""""""""""""""""

" new command ':W' to write as file as root
command! W w !sudo tee % > /dev/null

augroup common
    " clear all auto commands for this group
    autocmd!

    " reomve all trailing spaces when buffer is written
    autocmd BufWritePre * %s/\s\+$//e
augroup END

augroup ft_specials
    " clear all auto commands for this group
    autocmd!

    " remove tab to space conversion for make files
    autocmd FileType make setlocal noexpandtab

    " indent 2 spaces for VHDL
    autocmd FileType vhdl setlocal tabstop=2 shiftwidth=2

    " indent 2 spaces for Verilog
    autocmd FileType verilog setlocal tabstop=2 shiftwidth=2

    " indent 2 spaces for SystemVerilog
    autocmd FileType systemverilog setlocal tabstop=2 shiftwidth=2

    " indent 2 spaces for markdown and limit text width
    autocmd FileType markdown setlocal tabstop=2 shiftwidth=2 textwidth=79

    " handle do files as tcl files for ModelSim instead of stata
    autocmd BufRead,BufNewFile *.do setlocal ft=tcl

    " if the quickfix window is opened the window will be placed as the very
    " bottom split (without this, the quickfix may be the split of the right
    " tagbar window)
    autocmd FileType qf wincmd J

    " the quickfix buffer should not be accessable with :bn or :bp.
    " Therefore remove it from buffer list
    autocmd FileType qf set nobuflisted
augroup END


""""""""""""""""""""""""""""""""""""""""""""""""""
" Substituion with externals
""""""""""""""""""""""""""""""""""""""""""""""""""

" Use Ag over Grep
if executable('ag')
    set grepprg=ag\ --vimgrep\ $*
    set grepformat=%f:%l:%c:%m
endif


""""""""""""""""""""""""""""""""""""""""""""""""""
" Interfaces to externals
""""""""""""""""""""""""""""""""""""""""""""""""""

" Use cscope
if has('cscope')

    " Vim will include the cscope database whenever we search for a tag
    set cscopetag

    " gives us a success/failure message when trying to add a cscope database
    set cscopeverbose

    if has('quickfix')
        " see :help cscopequickfix
        set cscopequickfix=s-,c-,d-,i-,t-,e-
    endif

    " some abbreviations for the command line
    cnoreabbrev <expr> csa ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs add'  : 'csa')
    cnoreabbrev <expr> csf ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs find' : 'csf')
    cnoreabbrev <expr> csk ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs kill' : 'csk')
    cnoreabbrev <expr> csr ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs reset' : 'csr')
    cnoreabbrev <expr> css ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs show' : 'css')
    cnoreabbrev <expr> csh ((getcmdtype() == ':' && getcmdpos() <= 4)? 'cs help' : 'csh')

endif

""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""
"                                                "
"                 Plugins                        "
"                                                "
""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDtree
""""""""""""""""""""""""""""""""""""""""""""""""""

" ctrl + n toogles the NERDtree
nnoremap <C-n> :NERDTreeToggle<CR>

augroup nerdtree
    " clear all auto commands for this group
    autocmd!

    " close vim, if NERDTree is active, and the last buffer is closed
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
augroup END

""""""""""""""""""""""""""""""""""""""""""""""""""
" bufferline
""""""""""""""""""""""""""""""""""""""""""""""""""

" disable buffer line, because airline prints the buffers in the
" tab line, if only one tab is opend
let g:bufferline_echo = 0

""""""""""""""""""""""""""""""""""""""""""""""""""
" airline
""""""""""""""""""""""""""""""""""""""""""""""""""

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" unicode symbols
"let g:airline_left_sep = '>'
"let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
"let g:airline_right_sep = '<'
"let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.linenr = '¶'
let g:airline_symbols.linenr = ''
"let g:airline_symbols.maxlinenr = '☰'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
"let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.spell = '✎'
"let g:airline_symbols.notexists = '∄'
let g:airline_symbols.notexists = '∉'
"let g:airline_symbols.whitespace = 'Ξ'
let g:airline_symbols.whitespace = '␣'

" enable/disable detection of whitespace errors.
let g:airline#extensions#whitespace#enabled = 1
" configure which whitespace checks to enable. >
" indent: mixed indent within a line
" long:   overlong lines
" trailing: trailing whitespace
" mixed-indent-file: different indentation in different lines
let g:airline#extensions#whitespace#checks = [ 'indent', 'trailing', 'long', 'mixed-indent-file' ]

" if only one tab is used, the tab line shows the buffers
" if a second tab is used, the tab line is beautifed
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = '▶'
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#rigth_sep = '◀'
let g:airline#extensions#tabline#rigth_alt_sep = '|'
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#fnamemod = ':t' " show file name only

" color scheme of the airline status bar
"let g:airline_theme = 'base16_default'
let g:airline_theme = 'solarized'

"
" external plugins integration in airline
"

" integrate bufferline plugin into status line of airline plugin ...
"let g:airline#extensions#bufferline#enabled = 1
" ... and therefore deactive the buffer line in the command bar
"let g:bufferline_echo = 0
"
" don't integrate bufferline plugin into status line of airline plugin
let g:airline#extensions#bufferline#enabled = 0


" integrate fugitive plugin into status line of airline plugin
" shows the branch name if file is under version control
"let g:airline#extensions#branch#enabled = 1
"
" don't integrate fugitive plugin into status line of airline plugin
let g:airline#extensions#branch#enabled = 0


" integrate gitgutter plugin into status line of airline plugin
let g:airline#extensions#hunks#enabled = 1
" enable/disable showing only non-zero hunks.
let g:airline#extensions#hunks#non_zero_only = 0
" set hunk count symbols.
let g:airline#extensions#hunks#hunk_symbols = ['+', '~', '-']


" integrate tagbar plugin into status line of airline plugin
let g:airline#extensions#tagbar#enabled = 1
" show the full tag hierarchy
let g:airline#extensions#tagbar#flags = 'f'
" show function signature
" let g:airline#extensions#tagbar#flags = 's'
" show the raw prototype instead of parsed tag
" let g:airline#extensions#tagbar#flags = 'p'


" integrate zoomwintab plugin into status line of airline plugin
"
" enable/disable zoomwintab integration
let g:airline#extensions#zoomwintab#enabled = 1
" zoomwintab's zoomin symbol
"let g:airline#extensions#zoomwintab#status_zoomed_in = 'Currently Zoomed In'
" zoomwintab's zoomout symbol
"let g:airline#extensions#zoomwintab#status_zoomed_out = 'Currently Zoomed Out'


""""""""""""""""""""""""""""""""""""""""""""""""""
" GitGutter is removed by Signify
""""""""""""""""""""""""""""""""""""""""""""""""""

"nnoremap <C-Right> :GitGutterStageHunk<CR>
"nnoremap <C-Left> :GitGutterUndoHunk<CR>
"nnoremap <C-Up> :GitGutterPrevHunk<CR>
"nnoremap <C-Down> :GitGutterNextHunk<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""
" Signify
""""""""""""""""""""""""""""""""""""""""""""""""""

" manually choose supported version control systems
let g:signify_vcs_list = [ 'git', 'svn' ]
let g:signify_sign_add               = '+'
let g:signify_sign_delete            = '_'
let g:signify_sign_delete_first_line = '‾'
let g:signify_sign_change            = '~'
let g:signify_sign_changedelete      = g:signify_sign_change
let g:signify_update_on_bufenter     = 1
let g:signify_update_on_focusgained  = 1
"
" use arrow keys to jump between hunks
" don't use 'nnoremap' because it's not passed to an other command
nmap <C-Up> <plug>(signify-prev-hunk)
nmap <C-Down> <plug>(signify-next-hunk)

""""""""""""""""""""""""""""""""""""""""""""""""""
" settings for CtrlP
""""""""""""""""""""""""""""""""""""""""""""""""""

"Use this option to change the mapping to invoke CtrlP in |Normal| mode: >
let g:ctrlp_map = '<c-p>'

" Change the position, the listing order of results, the minimum and the maximum
" heights of the match window: >
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:10,results:50'

" enable search in tag file
let g:ctrlp_extensions = ['tag', 'buffertag']

" enable opening multiple files
" and open all files i hidden buffers
let g:ctrlp_open_multiple_files = 'i'

" Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
if executable('ag')
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
    " ag is fast enough that CtrlP doesn't need to cache
    let g:ctrlp_use_caching = 0
endif


""""""""""""""""""""""""""""""""""""""""""""""""""
" settings for ACK
""""""""""""""""""""""""""""""""""""""""""""""""""

" use ag instead of ack executable
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif


""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar
""""""""""""""""""""""""""""""""""""""""""""""""""
noremap <C-c> :TagbarToggle<CR>

" sort tags in according to their order in source file
let g:tagbar_sort = 0

" kind definitions of the universal ctags VHDL parser
"let g:tagbar_type_vhdl = {
"    \ 'ctagstype': 'vhdl',
"    \ 'kinds' : [
"        \'c:constants',
"        \'t:types',
"        \'T:subtypes',
"        \'r:records',
"        \'e:entities',
"        \'C:components',
"        \'d:prototypes',
"        \'f:functions',
"        \'p:procedures',
"        \'P:packages',
"        \'l:locals'
"    \]
"\}

" overriden kind extension via user .ctags config
let g:tagbar_type_vhdl = {
    \ 'ctagstype': 'vhdl',
    \ 'kinds' : [
        \'o:ports',
        \'s:signals',
        \'c:constants',
        \'t:types',
        \'T:subtypes',
        \'r:records',
        \'e:entities',
        \'C:components',
        \'d:prototypes',
        \'f:functions',
        \'p:procedures',
        \'P:packages',
        \'l:locals',
        \'x:processes',
        \'i:instances'
    \]
\}

" copied from tagbar wiki https://github.com/majutsushi/tagbar/wiki
" not verified against universal ctags parser if all this kinds
" are still valid
let g:tagbar_type_systemverilog = {
    \ 'ctagstype': 'systemverilog',
    \ 'kinds' : [
         \'A:assertions',
         \'C:classes',
         \'E:enumerators',
         \'I:interfaces',
         \'K:packages',
         \'M:modports',
         \'P:programs',
         \'Q:prototypes',
         \'R:properties',
         \'S:structs and unions',
         \'T:type declarations',
         \'V:covergroups',
         \'b:blocks',
         \'c:constants',
         \'e:events',
         \'f:functions',
         \'m:modules',
         \'n:net data types',
         \'p:ports',
         \'r:register data types',
         \'t:tasks',
     \],
     \ 'sro': '.',
     \ 'kind2scope' : {
        \ 'K' : 'package',
        \ 'C' : 'class',
        \ 'm' : 'module',
        \ 'P' : 'program',
        \ 'I' : 'interface',
        \ 'M' : 'modport',
        \ 'f' : 'function',
        \ 't' : 'task',
     \},
     \ 'scope2kind' : {
        \ 'package'   : 'K',
        \ 'class'     : 'C',
        \ 'module'    : 'm',
        \ 'program'   : 'P',
        \ 'interface' : 'I',
        \ 'modport'   : 'M',
        \ 'function'  : 'f',
        \ 'task'      : 't',
     \ },
     \}

" kind definitions of the universal ctags MARKDOWN parser
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'kinds' : [
         \'c:chapter',
         \'s:section',
         \'S:subsection',
         \'t:subsubsection',
         \'T:l4subsection',
         \'u:l5subsection',
     \]
\}
"
""""""""""""""""""""""""""""""""""""""""""""""""""
" Current Word
""""""""""""""""""""""""""""""""""""""""""""""""""

" Enable/disable plugin
let g:vim_current_word#enabled = 1

" Twins of word under cursor:
let g:vim_current_word#highlight_twins = 1
" The word under cursor:
let g:vim_current_word#highlight_current_word = 1

" Setting this option to more than 0 will enable delayed highlighting. The
" value of this variable is a delay in milliseconds.
" let g:vim_current_word#highlight_delay = 0o

" Disabling this option will make the word highlight persist over window
" switches and even over focusing different application window.
" let g:vim_current_word#highlight_only_in_focused_window = 1

" To avoid specific filetypes, add this variable to your vimrc:
" let g:vim_current_word#excluded_filetypes = ['ruby']

" To prevent the plugin from running in one or more buffers add following to
" your vimrc:
" The example below disables the plugin in:
" - Every buffer which name start with NERD_tree_
" - Every buffer which name equals your_buffer_name.rb
" - Every buffer which name ends with .js
" autocmd BufAdd NERD_tree_*,your_buffer_name.rb,*.js :let b:vim_current_word_disabled_in_this_buffer = 1

" Change highlight style of twins of word under cursor:
" hi CurrentWordTwins guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
"                            └┴┴┴┴┤        └┴┴┴┴┤     └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤         └┴┤         └┴┤       └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤
"      gui-vim font color hex code│             │   gui-vim special styles│           │           │ console-vim special styles│
"      ───────────────────────────┘             │   ──────────────────────┘           │           │ ──────────────────────────┘
"              gui-vim background color hex code│     console-vim font term color code│           │
"              ─────────────────────────────────┘     ────────────────────────────────┘           │
"                                                           console-vim background term color code│
"                                                           ──────────────────────────────────────┘

" Change highlight style of the word under cursor:"
" hi CurrentWord guifg=#XXXXXX guibg=#XXXXXX gui=underline,bold,italic ctermfg=XXX ctermbg=XXX cterm=underline,bold,italic
"                       └┴┴┴┴┴──┐     └┴┴┴┴┤     └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤         └┴┤         └┴┤       └┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┴┤
"    gui-vim font color hex code│          │   gui-vim special styles│           │           │ console-vim special styles│
"    ───────────────────────────┘          │   ──────────────────────┘           │           │ ──────────────────────────┘
"         gui-vim background color hex code│     console-vim font term color code│           │
"         ─────────────────────────────────┘     ────────────────────────────────┘           │
"                                                      console-vim background term color code│
"                                                      ──────────────────────────────────────┘"

" Example
" hi CurrentWord ctermbg=53
" hi CurrentWordTwins ctermbg=237"

" Just underline word under cursor to avoid highlighting
" as it would be using * or #
hi CurrentWord gui=underline cterm=underline
