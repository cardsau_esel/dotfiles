" Vim compiler file
" Compiler: Compiler Suite from Synopsys VCS
" Maintainer:
" Latest Revision:

if exists("current_compiler")
  finish
endif
let current_compiler = "vcs"

if exists(":CompilerSet") != 2
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo&vim

CompilerSet makeprg=make

" clear
CompilerSet errorformat=

" set directory stack tracing.
CompilerSet errorformat+=%Dmake:\ Entering\ directory\ %[`']%f'
CompilerSet errorformat+=%Xmake:\ Leaving\ directory\ %[`']%f'

" call make in make
CompilerSet errorformat+=%Dmake[%\\d%\\+]:\ Entering\ directory\ %[`']%f'
CompilerSet errorformat+=%Xmake[%\\d%\\+]:\ Leaving\ directory\ %[`']%f'

" vlogan: file not found
CompilerSet errorformat+=%E%>%trror-[SFCOR]\ Source\ file\ cannot\ be\ opened,
                        \%C%m,
                        \%C%m,
                        \%Z%m

" vhdlan: file not found
CompilerSet errorformat+=%E%>%trror:\ analysis\ Parsing\ vhdl-783,
                        \%C%m,
                        \%Z%m,

" generic Error (vlogan/vhdlan)
CompilerSet errorformat+=%E%>%trror-[%\\S%\\+]\ %m,
                        \%Z%f\\,\ %l

" generic Warning
CompilerSet errorformat+=%W%>%tarning-[%\\S%\\+]\ %m,
                        \%Z%f\\,\ %l

let &cpo = s:cpo_save
unlet s:cpo_save
